package ru.tsc.pavlov.tm.model;

public class Command {

    private final String name;

    private final String argument;

    private final String description;

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getArgument() {
        return argument;
    }

    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " ";
        if (argument != null && !argument.isEmpty()) result += "(" + argument + ")";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}

