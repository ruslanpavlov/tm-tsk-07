package ru.tsc.pavlov.tm.repository;

import ru.tsc.pavlov.tm.constant.ArgumentConst;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.model.Command;

public class CommandRepository {
    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO, "info - display system information"
    );
    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null, "close application"
    );

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "about - display developer info"
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, "version - display program info"
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, "help - display list of commands"
    );

    public static final Command[] COMMANDS = new Command[]{
            INFO, ABOUT, VERSION, HELP, EXIT
    };

    public static Command[] getCommands() {
        return COMMANDS;
    }
}
